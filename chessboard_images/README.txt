The default piece images are obtained from:
  http://en.wikipedia.org/wiki/Template:Chess_image
They are made from xboard (GPL-ed) and are freely redistributable.
The border images are made by vyvee.
