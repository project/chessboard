This module renders chessboard diagrams specified with the FEN syntax
or a simple piece placement format.
