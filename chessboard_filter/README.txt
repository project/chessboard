The Chessboard Filter module adds a chessboard diagram filter to your site, for
use with text formats. This filter adds the ability to use chessboard diagrams
in any text field that uses a text format (such as the body of a content item or
the text of a comment).
Help is available on your admin/help/chessboard_filter page.
